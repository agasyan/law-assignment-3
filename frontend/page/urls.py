from django.urls import path
from django.conf.urls import include, url

from page import views


#url for app
urlpatterns = [
    #functions
    url(r'^$', views.index, name='index'),
    url(r'^upload/$', views.upload, name='upload'),
]