import requests
import json
import string

from django.shortcuts import render, redirect
import uuid

# URL
BACKEND_LOCAL = 'http://localhost:8002/upload/'
BACKEND_PRODUCTION = 'http://152.118.148.95:20305/upload/'


def index(request):
    return render(request, 'index.html')


def upload(request):
    if request.method == "POST":
        try:
            routing_id = str(uuid.uuid4())
            inputfile = request.FILES.get('rawfile')
            # if tidak kebaca
            if inputfile == None:
                msg = 'File not on server'
                status = 'error'
                # if error return to index
                return render(request, 'progress.html', {'status': status, 'msg':msg, 'routing': routing_id, 'response': 'gagal'})
            msg = 'File success read by server 1'
            status = 'ok'
            name = format_filename(inputfile.name)
            # Send Post
            response = requests.post(BACKEND_PRODUCTION, headers={
                'X-ROUTING-KEY': routing_id,
            }, files = {
                'file' : (name, inputfile.read()),
            })
            parsed_json = response.json()

            # print(parsed_json)
            return render(request, 'progress.html', {'status': status, 'msg':msg, 'routing': routing_id, 'response': parsed_json["status"]})
        except Exception as e:
            msg = 'there was an error on file upload server 1, detail: "' + repr(e) +'"'
            # print(msg)
            status = 'error'
            # if error return to index
            return render(request, 'progress.html', {'status': status, 'msg':msg, 'response': 'gagal'})
    else:
        # if method not post return to index
        return redirect(f'/')

def format_filename(s):
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    filename = ''.join(c for c in s if c in valid_chars)
    filename = filename.replace(' ','_') # I don't like spaces in filenames.
    return filename