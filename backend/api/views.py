# Import
import requests
from django.http import HttpResponse, JsonResponse
from django.core.files.storage import FileSystemStorage

# RabbitMQ
import pika
import sys

# Threading
from threading import Thread

# PATH
import os.path
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

# Zip File Library
from pathlib import Path
import zipfile
import types
import os
from functools import partial
from os.path import basename

# REST
from rest_framework.response import Response
#for function based views
from rest_framework.decorators import api_view
#for class based views
from rest_framework.views import APIView
#for apiroot reverse
from rest_framework.reverse import reverse


# CONSTANTS
NPM = '1606918396'
USER = '0806444524'
PASS = '0806444524'
PORT_RABBITMQ = 5672
HOST_RABBITMQ = "152.118.148.95"
VIRTUAL_HOST = '/0806444524'

# Create your views here.

@api_view(['POST'])
def upload_file(request):
    if request.method == 'POST':
        try:
            # Get KEY
            routing = request.headers.get('X-ROUTING-KEY', '')

            # Save Raw File to media
            rawfile = request.FILES.get('file')
            if rawfile == None:
                return JsonResponse({'message': 'Your File Sended is empty', 'status': 'error'}, status = 500)
            fs = FileSystemStorage()
            filename = fs.save(rawfile.name, rawfile)

            # Input output file path
            newfile_path = SITE_ROOT+ "/.." + fs.url(filename)
            out_name = 'media/zip/' + os.path.splitext(filename)[0] + '_zipped.zip'
            
            thread_compress_bg(newfile_path, out_name, routing)
            return JsonResponse({'message': 'Your File Accepted Succesfully', 'status': 'accepted'}, status = 200)
        
        except Exception as e:
            return JsonResponse({'message': 'there was an error on upload file Function, detail: "' + repr(e) +'"', 'status': 'error'}, status = 500)

    else:
        return JsonResponse({'message': 'Not POST Method', 'staus': 'error'})

def send_mq(msg, routing):
    connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=HOST_RABBITMQ, 
        port=PORT_RABBITMQ,
        virtual_host=VIRTUAL_HOST, 
        credentials=pika.PlainCredentials(USER, PASS)))

    channel = connection.channel()
    channel.exchange_declare(exchange=NPM, exchange_type='direct')
    channel.basic_publish(exchange=NPM, routing_key=routing, body=str(msg))

def compress_file(in_file, out_file, routing_key):
    progress.bytes = 0
    progress.obytes = 0
    progress.step = 0
    progress.routing = routing_key
    with zipfile.ZipFile(out_file, 'w', compression=zipfile.ZIP_DEFLATED) as _zip:
        # Replace original write() with a wrapper to track progress
        _zip.fp.write = types.MethodType(partial(progress, os.path.getsize(in_file), _zip.fp.write), _zip.fp)
        _zip.write(in_file, basename(in_file))
    send_mq(str(out_file), progress.routing)

def progress(total_size, original_write, self, buf):
    progress.bytes += len(buf)
    progress.obytes += 1024 * 8  # Hardcoded in zipfile.write
    real = int(100 * progress.obytes / total_size)
    if progress.step >= 100 and progress.step < 200:
        print("100 % done")
        send_mq(str(progress.step), progress.routing)
        # Break loop
        progress.step += 1000
    elif progress.step < real and progress.step <= 100:
        print("{} % done".format(int(100 * progress.obytes / total_size)))
        send_mq(str(progress.step), progress.routing)
        progress.step += 10    
    return original_write(buf)

def thread_compress_bg(in_file, out_file, routing_key):
    compress_thread = Thread(target = compress_file, args = (in_file, out_file, routing_key, ))
    compress_thread.start()

    