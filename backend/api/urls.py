from django.urls import path
from django.conf.urls import include, url

from api import views


#url for app
urlpatterns = [
    #functions
    url(r'^upload/$', views.upload_file, name='upload-file'),
]